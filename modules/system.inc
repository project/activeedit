<?php

/**
 * Implementation of hook_activeedit_elements().
 */
function system_activeedit_elements($object = NULL) {
  $elements = array();
  // Test for appropriate permissions.
  $access = user_access('administer site configuration');
  $elements['site_mission'] = array(
    '#title' => t('site mission'),
    '#selector' => '#mission',
    '#url' => 'admin/settings/site-information',
    '#access' => $access,
    '#form' => array(
      'system_site_information_settings' => array(
        array(
          'site_mission',
        ),
      ),
    ),
  );
  $elements['site_footer'] = array(
    '#title' => t('footer'),
    '#selector' => '#footer',
    '#url' => 'admin/settings/site-information',
    '#access' => $access,
    '#form' => array(
      'system_site_information_settings' => array(
        array(
          'site_footer',
        ),
      ),
    ),
  );
  // We generate content separately, on demand, to avoid unneeded processing.
  if (isset($_REQUEST['activeedit_element']) && $key = $_REQUEST['activeedit_element']) {
    $content = FALSE;
    switch ($key) {
      case 'site_mission':
      case 'site_footer':
        $content = variable_get($key, '');
        break;
    }
    if ($content) {
      $elements[$key]['#content'] = $content;
    }
  }

  return $elements;
}