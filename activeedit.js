
Drupal.behaviors.activeEdit = function (context) {
  $('body:not(.activeedit-processed)')
      .addClass('activeedit-processed')
      .prepend('<div class="activeedit-enable"><label><input class="activeedit-enable" type="checkbox" /> '+ Drupal.t('Active editing mode') +'</label></div>')
      .find('input.activeedit-enable')
      .click(function () {
        var checked = $(this).is(':checked');
        $.each(Drupal.settings.activeEdit, function (key, value) {
          var delimiter = value['#url'].indexOf('?') == -1 ? '?' : '&';
          $(value['#selector']).each(function () {
            var a = document.createElement('a');
            $(a)
              .attr('href', value['#url'] + delimiter +'activeedit_element='+ key)
              .addClass('activeedit');
            if (checked) {
              var link = $(this)
                .wrap(a)
                .mouseover(function() {
                  $(this).addClass('activeedit-active');
                })
                .mouseout(function() {
                  $(this).removeClass('activeedit-active');
                })
                .parent('a.activeedit')
                .get(0);
              Drupal.behaviors.popups(document);
            }
            else {
              $(this)
                .unbind('mouseover')
                .unbind('mouseout')
                .parent('a')
                .replaceWith(this);
            }
          });
      });
  });
};
