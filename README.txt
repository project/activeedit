Drupal activeedit.module README.txt
==============================================================================

Provides Ajax-based editing.

Active edit is being rewritten for 6.x with many fewer features. It is 
incomplete and currently only useful for demo or testing purposes.

Technical details: Implemented through hook_activeedit_elements(), which returns
an array of elements to attach behaviors to. Elements are identified by their
jQuery selectors, which can be specified by theme (see files in the /themes
directory). 

Requirements
------------------------------------------------------------------------------
This module is written for Drupal 5.0+ and requires the popups and formfilter
modules to be enabled.

Implementation details
------------------------------------------------------------------------------
The approach is different from most in place editing implementations. Activeedit
uses a helper module, Formfilter, to fetch altered versions of the original
forms. That is, when we want to change a setting (e.g., the site name), we fetch
the appropriate settings form via--but alter it so it has only the field we
need. Then we submit that form via AJAX to its normal path.

The key advantage is that we can don't need to reinvent. Permissions/access
issues are already handled. We can use the original forms so we don't need to
replicate them.

Developer Usage
-----------------------------------------------------------------------------

New activeedit behaviours can be easily attached to page elements through
implementations of three activeedit hooks: hook_activeedit_elements() (for
page elements to make editable); hook_activeedit_links() (for links to bring
up forms for adding items); and hook_activeedit_autocompletes() (for 
autocomplete elements to enable new item adding for.

See the include files in the 'modules' directory of the activeedit module
for sample implementations.

Theme support for in place editing
----------------------------------

Activeedit is theme-dependent in that the Javascript needs to be able to
identify which elements to attach to.

Elements are identified by jQuery selectors, see

http://www.jquery.com/DOM/Traversing/Selectors

Each element is given a default selector, but individual themes may or may not
have the same HTML structure as the default.

To add support for a theme, you need to write a themename.inc file and put it
in the module's theme directory (where themename is the name of the theme). 
See existing theme include files for examples.

The items in a theme .inc file represent those elements that have a different
selector in the theme than the default.

The helper module activeedit_theme_assist is available for generating a stub
theme include file. This module is not included in the stable release of
jstools but is in CVS in the activeedit directory. Install the module and 
visit admin/settings/activeedit_theme_assist to get a stub include file
plus some basic instructions.


